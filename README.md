# Gradient Descent for Univariate Non-Linear Regression
# and Multivariate Logistic Regression
Program in Java to demonstrate univariate non-linear regression and multivariate
logistic regression with gradient descent.

## Usage
On Linux or MacOS:
```sh
$ chmod +x gradient-descent.sh
$ ./gradient-descent.sh
```

On Windows:
```bat
C:\DIRECTORY> gradient-descent.bat
```
These will automatically compile and run the program.