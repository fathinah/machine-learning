import java.util.List;
import java.lang.Math;

public class GradientDescentClassification {
    //Read the pdf file for further explanation
    //Importing CSV file from data folder as our training data
    public static final String DATA_FILE = "data/data_2.csv";

    public static void main(String[] args) {

        // -------------------------------------------------
        // Set up the csv file into a list to get executable data to be plotted later into a graph
        // Plotting data points into a graph using Plot class
        // -------------------------------------------------
        List<List<Double>> data = Data.dataFrom(DATA_FILE);
        Plot plt = new Plot("Univariate non-linear Regression", "X1", "X2", data);

        // -------------------------------------------------
        // Multivariate Logistic Regression Using The Gradient Descent
        // -------------------------------------------------
        final int epochs = 100000;  // Number of iterations we want to run by the algorithm

        // We want to classify hw(x) = g(z) = 1/(1+e^-(w2*x2 + w1 * x1 + w0))
        double w2 = 0;
        double w1 = 0;
        double w0 = 0;

        // Learning rate as a constant to pick the next step ( data point ) to visit
        double alpha =6;

        // Main Gradient Descent Function for Multivariate Logistic Regression
        for (int i = 0; i < epochs; i++) {

            double cost = 0;

            //Iteration to every data points to calculate the cost function with certain w0,w1,w2 values.
            for (int j = 0; j < data.get(0).size(); j++) {

                double x1 = data.get(0).get(j);      //get the independent variable of data
                double x2 = data.get(1).get(j);      //get the second independent variable of data
                double y = data.get(2).get(j);       //get the dependent variable of data


                double prediction = (w2 * x2) + (w1 * x1) + w0; //the z value, which will be converted into 0-1 using the sigmoid function
                double sigmoid = 1 / (1 + Math.exp(-1 * prediction)); //hw(x), the prediction line


                // cost function for logistic regression problem.
                // -log( hw(X) ) if y = 1
                // -log( 1 - hw(X) ) if y = 0
                cost += (y * Math.log(sigmoid) + (1 - y) * (Math.log(1 - sigmoid)));

                // Update the parameters for our equation.
                // We need to add residuals from all data points ( the sigma ) multiplied with alpha and x accordingly
                // - which produce the final w2,w1,w0 values(in one epoch) after the cost function ( the inner loop iteration ) completed
                w2 += alpha * (y - sigmoid) * x2;
                w1 += alpha * (y - sigmoid) * x1;
                w0 += alpha * (y - sigmoid);

            }

            //Regularizing the cost to get a better approximation
            //Which constants that give us the lowest cost is what this algorithm is all about.
            cost = -1 * cost / (data.get(0).size());

            System.out.println("Current Cost: " + cost);

            // ---------------------------------------------
            // Our Hypothesis Function after the epoch
            // (these values are final because of how
            // functional programming works in Java).
            final double w_2 = w2;
            final double w_1 = w1;
            final double w_0 = w0;
            HypothesisFunction h_x = (x) -> (-1 * w_1 * x - w_0) / w_2;
            // ----------------------------------------------
            // Plotting predicted decision boundary with current values of w
            // There will be sum (epochs) different plots until we reach the final decision boundary
            plt.updatePlot(h_x);
            // ----------------------------------------------
        }


        System.out.println("Final Equation: h(x) = (" + w2 + " x2 ) + (" + w1 + " * x1) + " + w0);
    }
}
