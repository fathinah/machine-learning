import java.util.List;

public class GradientDescentRegression {
    //Read the pdf file for further explanation
    //Importing CSV file from data folder as our training data
    public static final String DATA_FILE = "data/input_1.csv";

    public static void main(String[] args) {

        // -------------------------------------------------
        // Set up the csv file into a list to get executable data to be plotted later into a graph
        // Plotting data points into a graph using Plot class
        // -------------------------------------------------
        List<List<Double>> data = Data.dataFrom(DATA_FILE);
        Plot plt = new Plot("Univariate non-linear Regression", "X", "Y", data);

        // -------------------------------------------------
        // Univariate Non-Linear Regression Using The Gradient Descent
        // -------------------------------------------------
        final int epochs = 1000000;  // Number of iterations we want to run by the algorithm

        // We want to predict hw(x) = w2*x^2 +  w1 * x + w0
        double w2 = 0;
        double w1 = 0;
        double w0 = 0;

        // Learning rate as the constant to pick the next step ( data point ) to visit
        double alpha = 0.00000001;

        // Main Gradient Descent Function for Non-Linear Regression
        for (int i = 0; i < epochs; i++) {

            double cost = 0;

            //Iteration to every data points to calculate the cost function with certain w0,w1,w2 values.
            for (int j = 0; j < data.get(0).size(); j++) {

                double x_j = data.get(0).get(j);    //get the independent variable of data
                double y_j = data.get(1).get(j);    //get the dependent variable of data

                double prediction = (w2 * (x_j * x_j)) + (w1 * x_j) + w0;           //hw(x), the prediction line

                // cost += (y_j - h(x))^2 . Cost function uses the Sum of Residual Square or L2
                cost += ((y_j - prediction) * (y_j - prediction));

                // Update the parameters for our equation.
                // We need to add residuals from all data points ( the sigma ) multiplied with alpha and x, which we get from derivative of L2 function
                // - which produce the final w2,w1,w0 values(in one epoch) after the cost function ( the inner loop iteration ) completed
                w2 += alpha * (y_j - prediction) * (x_j * x_j);
                w1 += alpha * (y_j - prediction) * x_j;
                w0 += alpha * (y_j - prediction);
            }

            //Cost is our main focus. Which constants that give us the lowest cost is what this algorithm is all about.
            System.out.println("Current Cost: " + cost);


            // ---------------------------------------------
            // Our Hypothesis Function after the epoch
            final double w_2 = w2;
            final double w_1 = w1;
            final double w_0 = w0;
            HypothesisFunction h_x = (x) -> (w_2 * (x * x)) + (w_1 * x) + w_0;
            // ----------------------------------------------
            // Plotting prediction with current values of w
            // There will be sum (epochs) different plots until we reach the final prediction line
            plt.updatePlot(h_x);

            // ----------------------------------------------
        }


        System.out.println("Final Equation: h(x) = (" + w2 + " x^2 ) + (" + w1 + " * x) + " + w0);
    }
}

// some part of the code inspired by Alfi Sumadi : https://github.com/alfi-s/univariate-linear-regression
